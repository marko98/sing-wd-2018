(function(d){
    var knjige = JSON.parse(window.localStorage.getItem("knjige"));
    var autori = JSON.parse(window.localStorage.getItem("autori"));
    var prviAutor = autori[0].ime + " " + autori[0].prezime;

    var selectAutori = d.getElementById("selectChooseAnAuthor");
    for(autor of autori){
        var optionAutor = d.createElement("option");
        optionAutor.innerHTML = autor.ime + " " + autor.prezime;
        optionAutor.value = autor.ime + " " + autor.prezime;
        selectAutori.appendChild(optionAutor);
    };

    novaLista = [];

    for (knjiga of knjige){
        var autor = knjiga.autor.ime + " " + knjiga.autor.prezime;
        if (autor == prviAutor){
            novaLista.push(knjiga);
        };
    };

    pPAPrikaziKnjige(novaLista);

})(document);

function pPAPretraga(){
    (function(d){
        var knjige = JSON.parse(window.localStorage.getItem("knjige"));
        var selectAutori = d.getElementById("selectChooseAnAuthor").value;
        novaLista = [];

        for (knjiga of knjige){
            var autor = knjiga.autor.ime + " " + knjiga.autor.prezime;
            if (autor == selectAutori){
                novaLista.push(knjiga);
            };
        };
    
        pPAPrikaziKnjige(novaLista);
    })(document)
};

function pPAPrikaziKnjige(upisaneKnjige){
    (function(d){
        let div = document.getElementById("divBoxBooks");
        div.innerHTML = "";
        for (let knjiga of upisaneKnjige){
            if(knjiga.obrisan == false){
                let prikaz = pPANapraviDivKnjige(knjiga);
                div.append(prikaz);
            };
        }
    })(document)
};

function pPANapraviDivKnjige(knjiga){
    let divKnjige = document.createElement("div");
    divKnjige.classList.add("book");
    (function(){
        // console.log("usao u prikaz");

        let slika = document.createElement("img");
        slika.src = knjiga.slika;
        slika.alt = knjiga.naziv;
        divKnjige.appendChild(slika);

        let naslov = document.createElement("h3");
        naslov.innerHTML = knjiga.naziv;
        naslov.classList.add("naslovKnjige");
        divKnjige.appendChild(naslov);

        divKnjige.onclick = function(){
            window.location.replace(`../templates/book.html?${knjiga.id}`);
        }
    })()
    return divKnjige;
};

function pPAPocetna(){
    (function(d){
        window.location.replace("../templates/main.html");
    })(document);
};