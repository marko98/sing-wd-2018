(function(d){
    var trenutniKorisnik = JSON.parse(window.localStorage.getItem("trenutniKorisnik"));
    d.getElementById("username").value = trenutniKorisnik.korisnickoIme;

})(document);

function pIIPIzmena(){
    (function(d){
        var password = d.getElementById("password").value;
        var trenutniKorisnik = JSON.parse(window.localStorage.getItem("trenutniKorisnik"));
        var registrovaniKorisnici = JSON.parse(window.localStorage.getItem("registrovaniKorisnici"));
        var registrovaniKorisnik = undefined;
        for(korisnik of registrovaniKorisnici){
            if(korisnik.korisnickoIme == trenutniKorisnik.korisnickoIme){
                registrovaniKorisnik = korisnik;
                break;
            };
        };

        if(password != trenutniKorisnik.sifra){
            for(korisnik of registrovaniKorisnici){
                if(korisnik.korisnickoIme == trenutniKorisnik.korisnickoIme){
                    korisnik.sifra = password;
                };
                break;
            };
            registrovaniKorisnik.sifra = password;
            trenutniKorisnik.sifra = password;
            window.localStorage.setItem("trenutniKorisnik", JSON.stringify(trenutniKorisnik));
            window.localStorage.setItem("registrovaniKorisnici", JSON.stringify(registrovaniKorisnici));
        };

    })(document);
};

function pIIPPocetna(){
    (function(d){
        window.location.replace("../templates/main.html");
    })(document);
};