function sIASignIn(){
    (function(d){
        var username = d.getElementById("username").value;
        var password = d.getElementById("password").value;

        if(JSON.parse(window.localStorage.getItem("registrovaniKorisnici")) == null){
            var registrovaniKorisnici = [];
            var korisnik = {
                "korisnickoIme": username,
                "sifra": password,
                "uloga": "admin"
            };
            registrovaniKorisnici.push(korisnik);
            window.localStorage.setItem("registrovaniKorisnici", JSON.stringify(registrovaniKorisnici));
            window.localStorage.setItem("trenutniKorisnik", JSON.stringify(korisnik));
            window.location.replace("../templates/main.html");
        } else {
            var registrovaniKorisnici = JSON.parse(window.localStorage.getItem("registrovaniKorisnici"));
            var upisi = true;
            for(korisnik of registrovaniKorisnici){
                if(korisnik.korisnickoIme == username){
                    upisi = false;
                };
            };
            if(upisi){
                var korisnik = {
                    "korisnickoIme": username,
                    "sifra": password,
                    "uloga": "admin"
                };
                registrovaniKorisnici.push(korisnik);
                window.localStorage.setItem("registrovaniKorisnici", JSON.stringify(registrovaniKorisnici));
                window.localStorage.setItem("trenutniKorisnik", JSON.stringify(korisnik));
                window.location.replace("../templates/main.html");
            } else {
                window.alert("Korisnicko ime je zauzeto. Pokusajte ponovo.");
            };
        };
    })(document)
};

function sIAPocetna(){
    (function(d){
        window.location.replace("../templates/main.html");
    })(document);
};