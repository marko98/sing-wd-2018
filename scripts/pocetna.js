function pChangeWindow(){
    (function(d){
        var member = d.getElementById("member").checked;
        var firstTime = d.getElementById("firstTime").checked;
        var admin = d.getElementById("admin").checked;

        if(member){
            window.location.replace("../templates/logovanjeKorisnika.html");
        } else if(firstTime){
            window.location.replace("../templates/signIn.html");
        } else if(admin){
            window.location.replace("../templates/logInAdmina.html");
        };
    })(document);
};

(function(d){
    if (!JSON.parse(window.localStorage.getItem("registrovaniKorisnici"))){
        var registrovaniKorisnici = [{
            "korisnickoIme": "david",
            "sifra": "david",
            "uloga": "admin"
        }];
        window.localStorage.setItem("registrovaniKorisnici", JSON.stringify(registrovaniKorisnici));
    }
})(document);