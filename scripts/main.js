(function(d){
    var trenutniKorisnik = JSON.parse(window.localStorage.getItem("trenutniKorisnik"))
    if (trenutniKorisnik.uloga == "admin"){
        mPrikazZaAdmina();
    }else{
        mPrikazZaKorisnika();
    };

    if (!JSON.parse(window.localStorage.getItem("zanrovi"))){
        var zanrovi = ["fantazija", "avantura", "komedija"];
        window.localStorage.setItem("zanrovi", JSON.stringify(zanrovi));
    }

    if (!JSON.parse(window.localStorage.getItem("autori"))){
        var autori = [{"ime":"J. K.", "prezime":"Rowling"}, {"ime":"Michael", "prezime":"Crichton"}];
        window.localStorage.setItem("autori", JSON.stringify(autori));
    }

    if (!JSON.parse(window.localStorage.getItem("knjige"))){
        knjige = [
            {
                "naziv": "Harry Potter and the Chamber of Secrets",
                "autor": {"ime":"J. K.", "prezime":"Rowling"},
                "zanr": "fantazija",
                "brStrana": 251,
                "id": 1,
                "godina": 2002,
                "obrisan": false,
                "cena": 1100,
                "kolicina": 50,
                "prica": "Hari Poter , siroče, tuguje, prezren od svih , u porodici neprijatnih rođaka. Neobična pisma, koja misteriozno stižu svakog dana, izmeniće sudbinu dečaka odvodeci ga u školu čarolija i vračanja, Hogvorts...",
                "slika": "../images/1.Harry Potter Books/Harry Potter and the Chamber of Secrets.jpg"
            },
            {
                "naziv": "Jurassic Park",
                "autor": {"ime":"Michael", "prezime":"Crichton"},
                "zanr": "avantura",
                "brStrana": 416,
                "id": 2,
                "godina": 1991,
                "obrisan": false,
                "cena": 950,
                "kolicina": 50,
                "prica": "An astonishing technique for recovering and cloning dinosaur DNA has been discovered. Creatures once extinct now roam Jurassic Park, soon-to-be opened as a theme park. Until something goes wrong...and science proves a dangerous toy....",
                "slika": "../images/4.Jurassic Park Books/Jurassic Park.jpg"
            },
            {
                "naziv": "The Lost World",
                "autor": {"ime":"Michael", "prezime":"Crichton"},
                "zanr": "komedija",
                "brStrana": 430,
                "id": 3,
                "godina": 1995,
                "obrisan": false,
                "cena": 960,
                "kolicina": 50,
                "prica": "An astonishing technique for recovering and cloning dinosaur DNA has been discovered. Creatures once extinct now roam Jurassic Park, soon-to-be opened as a theme park. Until something goes wrong...and science proves a dangerous toy....",
                "slika": "../images/4.Jurassic Park Books/The Lost World.jpg"
            }
        ];
        window.localStorage.setItem("knjige", JSON.stringify(knjige));
        var upisaneKnjige = JSON.parse(window.localStorage.getItem("knjige"));
        prikaziKnjige(upisaneKnjige);
    } else {
        var upisaneKnjige = JSON.parse(window.localStorage.getItem("knjige"));
        prikaziKnjige(upisaneKnjige);
    }

})(document)

function mPrikazZaKorisnika(){
    (function(d){
        var listaElemenata = [];

        var dodajAdmina = d.getElementById("noviAdmin");
        var novaKnjiga = d.getElementById("novaKnjiga");
        var noviAutor = d.getElementById("noviAutor");
        listaElemenata.push(dodajAdmina);
        listaElemenata.push(novaKnjiga);
        listaElemenata.push(noviAutor);
        for(element of listaElemenata){
            element.setAttribute("style", "display:none;");
        };
        
    })(document);
};

function mPrikazZaAdmina(){
    (function(d){
        var listaElemenata = [];

        var mojaKorpa = d.getElementById("mojaKorpa");
        var izmeniProfil = d.getElementById("izmeniProfil");
        listaElemenata.push(mojaKorpa);
        listaElemenata.push(izmeniProfil);
        
        for(element of listaElemenata){
            element.setAttribute("style", "display:none;");
        };
        
    })(document);
};

function mLogOut(){
    (function(d){
        window.localStorage.removeItem("trenutniKorisnik");
        window.location.replace("../templates/pocetna.html");
    })(document);
};

function mMojaKorpa(){
    (function(d){
        window.location.replace("../templates/korpa.html");
    })(document);
};

function mNovaKnjiga(){
    (function(d){
        window.location.replace("../templates/novaKnjiga.html");
    })(document);
};

function mIzmeniProfil(){
    (function(d){
        window.location.replace("../templates/prikazIIzmenaProfila.html");
    })(document);
};

function mDodajAdmina(){
    (function(d){
        window.location.replace("../templates/signInAdmin.html");
    })(document);
};

function mDodajAutora(){
    (function(d){
        window.location.replace("../templates/dodajAutora.html");
    })(document);
};

function mPretragaPoAutoru(){
    (function(d){
        window.location.replace("../templates/pretragaPoAutoru.html");
    })(document);
};

function prikaziKnjige(upisaneKnjige){
    (function(d){
        let div = document.getElementById("divLevaStrana");
        div.innerHTML = "";
        for (let knjiga of upisaneKnjige){
            if(knjiga.obrisan == false){
                let prikaz = napraviDivKnjige(knjiga);
                div.append(prikaz);
            };
        }
    })(document)
};

function napraviDivKnjige(knjiga){
    let divKnjige = document.createElement("div");
    divKnjige.classList.add("book");
    (function(){
        let slika = document.createElement("img");
        slika.src = knjiga.slika;
        slika.alt = knjiga.naziv;
        slika.className = "knjiga";
        divKnjige.appendChild(slika);

        let naslov = document.createElement("h3");
        naslov.innerHTML = knjiga.naziv;
        naslov.classList.add("naslovKnjige");
        divKnjige.appendChild(naslov);

        divKnjige.onclick = function(){
            window.location.replace(`../templates/book.html?${knjiga.id}`);
        }
    })()
    return divKnjige;
};

function mVidiYT(naziv){
    if(naziv == "hobit"){
        window.open(
            'https://www.youtube.com/embed/TpsFWBzDHkk',
            '_blank'
        );
    } else if(naziv == "starwars"){
        window.open(
            'https://www.youtube.com/embed/Q0CbN8sfihY',
            '_blank'
        );
    } else if(naziv == "hungerGames"){
        window.open(
            'https://www.youtube.com/embed/mfmrPu43DF8',
            '_blank'
        );
    };
};
function mPretraga(e){
    var pretragaPoNazivuKnjige = document.getElementById("inputNaslovKnjige").value;
    (function(d){
        var knjige = JSON.parse(window.localStorage.getItem("knjige"));
        var pronadjeneKnjige = [];
        for (knjiga of knjige){
            if (knjiga.naziv.toString().toLowerCase().includes(pretragaPoNazivuKnjige.toLowerCase())){
                pronadjeneKnjige.push(knjiga);
            };
        };
        if (pronadjeneKnjige.length == 0){
            window.alert("Nije pronadjena ni jedna knjiga koja odgovara Vasem unosu. Pokusajte ponovo.");
        } else {
            prikaziKnjige(pronadjeneKnjige);
        }
    })(document)
}