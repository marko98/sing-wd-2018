(function(d){
    let odabranaKnjiga = window.location.search.substring(1);
    let knjige = JSON.parse(window.localStorage.getItem("knjige"));
    var trenutniKorisnik = JSON.parse(window.localStorage.getItem("trenutniKorisnik"));

    var autori = JSON.parse(window.localStorage.getItem("autori"));
    var zanrovi = JSON.parse(window.localStorage.getItem("zanrovi"));

    var selectAutori = d.getElementById("selectChooseAnAuthor");
    var selectZanr = d.getElementById("selectZanr");


    var id = d.getElementById("id");
    id.value = odabranaKnjiga;

    for(knjiga of knjige){
        if(knjiga.id == odabranaKnjiga){
            d.getElementById("naziv").value = knjiga.naziv;
            d.getElementById("brStrana").value = knjiga.brStrana;
            d.getElementById("cena").value = knjiga.cena;
            d.getElementById("brPrimeraka").value = knjiga.kolicina;
            d.getElementById("godIzdavanja").value = knjiga.godina;

            var prviOption = d.createElement("option");
            prviOption.innerHTML = knjiga.zanr;
            prviOption.value = knjiga.zanr;
            selectZanr.appendChild(prviOption);
            for(zanr of zanrovi){
                if(zanr != knjiga.zanr){
                    var optionZanr = d.createElement("option");
                    optionZanr.innerHTML = zanr;
                    optionZanr.value = zanr;
                    selectZanr.appendChild(optionZanr);
                }
            };

            var optionAutorPrvi = d.createElement("option");
            var prviAutor = knjiga.autor.ime + " " + knjiga.autor.prezime;
            optionAutorPrvi.innerHTML = knjiga.autor.ime + " " + knjiga.autor.prezime;
            optionAutorPrvi.value = knjiga.autor.ime + " " + knjiga.autor.prezime;
            selectAutori.appendChild(optionAutorPrvi);
            for(autor of autori){
                autorKnjige = autor.ime + " " + autor.prezime;
                if(autorKnjige != prviAutor){
                    var optionAutor = d.createElement("option");
                    optionAutor.innerHTML = autor.ime + " " + autor.prezime;
                    optionAutor.value = autor.ime + " " + autor.prezime;
                    selectAutori.appendChild(optionAutor);
                };
            };

            d.getElementById("prica").value = knjiga.prica;
            var slikaKnjige = d.getElementById("slikaKnjige").src = knjiga.slika;
        };
    };


    if(trenutniKorisnik.uloga == "admin"){
        bPrikazZaAdmina();
    } else {
        bPrikazZaKorisnika();
    };
})(document);

function bPrikazZaKorisnika(){
    (function(d){
        var listaElemenata = [];

        var dodajSliku = d.getElementById("slika");
        var dodajZnak = d.getElementById("dodajZnak");
        var obrisiKnjigu = d.getElementById("obrisiKnjigu");
        var sacuvajDugme = d.getElementById("sacuvajDugme");
        listaElemenata.push(dodajSliku);
        listaElemenata.push(dodajZnak);
        listaElemenata.push(obrisiKnjigu);
        listaElemenata.push(sacuvajDugme);
        for(element of listaElemenata){
            element.setAttribute("style", "display:none;");
        };

        $("#naziv").attr("readonly", true); 
        $("#brStrana").attr("readonly", true); 
        $("#cena").attr("readonly", true); 
        $("#brPrimeraka").attr("readonly", true); 
        $("#godIzdavanja").attr("readonly", true); 

        var my_condition = true;
        var lastSel = $("#selectZanr option:selected");
        
        $("#selectZanr").change(function(){
          if(my_condition)
          {
            lastSel.prop("selected", true);
          }
        });
        
        $("#selectZanr").click(function(){
            lastSel = $("#selectZanr option:selected");
        });

        var my_condition2 = true;
        var lastSel2 = $("#selectChooseAnAuthor option:selected");
        
        $("#selectChooseAnAuthor").change(function(){
          if(my_condition2)
          {
            lastSel2.prop("selected", true);
          }
        });
        
        $("#selectChooseAnAuthor").click(function(){
            lastSel2 = $("#selectChooseAnAuthor option:selected");
        });

        $("#prica").attr("readonly", true); 

        var knjige = JSON.parse(window.localStorage.getItem("knjige"));

        for(knjiga of knjige){
            if(knjiga.id == window.location.search.substring(1)){
                var kolicina = d.getElementById("kolicina");
                kolicina.max = knjiga.kolicina;
                break;
            };
        };

        
        
    })(document);
};
function bPrikazZaAdmina(){
    (function(d){
        var listaElemenata = [];

        var divDesnaStranaDole = d.getElementById("divDesnaStranaDole");
        listaElemenata.push(divDesnaStranaDole);
        for(element of listaElemenata){
            element.setAttribute("style", "display:none;");
        };
        
    })(document);
};

var putanjaSlike = undefined;

function bDodajSliku(event){
    var selectedFile = event.target.files[0];
    var reader = new FileReader();
  
    var imgtag = document.getElementById("slikaKnjige");
    imgtag.title = selectedFile.name;
  
    reader.onload = function(event) {
        imgtag.src = event.target.result;
        putanjaSlike = event.target.result;
    };

    reader.readAsDataURL(selectedFile);
}

function bDodaj(){
    (function(d){
        var naziv = d.getElementById("naziv").value;
        var id = d.getElementById("id").value;
        var brStrana = d.getElementById("brStrana").value;
        var cena = d.getElementById("cena").value;
        var brPrimeraka = d.getElementById("brPrimeraka").value;
        var godIzdavanja = d.getElementById("godIzdavanja").value;
        var zanr = d.getElementById("selectZanr").selectedOptions[0].value;
        var selectChooseAnAuthor = d.getElementById("selectChooseAnAuthor").selectedOptions[0].value;
        selectChooseAnAuthor = 
        selectChooseAnAuthor.split(" ");
        selectChooseAnAuthor = {
            "ime": selectChooseAnAuthor[0],
            "prezime": selectChooseAnAuthor[1]
        };
        var prica = d.getElementById("prica").value;
        var knjige = JSON.parse(window.localStorage.getItem("knjige"));

        for(knjiga of knjige){
            if(knjiga.id == window.location.search.substring(1)){
                if(putanjaSlike == undefined){
                    putanjaSlike = knjiga.slika;
                };
                knjiga.naziv = naziv;
                knjiga.autor = selectChooseAnAuthor;
                knjiga.zanr = zanr;
                knjiga.brStrana = parseInt(brStrana);
                knjiga.id = parseInt(id);
                knjiga.godina = parseInt(godIzdavanja);
                knjiga.obrisan = false;
                knjiga.cena = parseInt(cena);
                knjiga.kolicina = parseInt(brPrimeraka);
                knjiga.prica = prica;
                knjiga.slika = putanjaSlike;

                window.localStorage.setItem("knjige", JSON.stringify(knjige));
                break;
            };
        };
        window.location.reload();
    })(document);
};

function bObrisiKnjigu(){
    (function(){
        if (confirm("Da li ste sigurni?")) {
            var knjige = JSON.parse(window.localStorage.getItem("knjige"));

            for(knjiga of knjige){
                if(knjiga.id == window.location.search.substring(1)){
                    knjiga.obrisan = true;
                    window.localStorage.setItem("knjige", JSON.stringify(knjige));
                    break;
                };
            };
            window.location.replace("../templates/main.html");
        } else {
        };
    })(document);
};

function bPocetna(){
    (function(d){
        window.location.replace("../templates/main.html");
    })(document);
};

function bDodajUKorpu(){
    (function(d){
        if(document.getElementById("kolicina").value == ""){
            return;
        };
        var registrovaniKorisnici = JSON.parse(window.localStorage.getItem("registrovaniKorisnici"));;
        var trenutniKorisnik = JSON.parse(window.localStorage.getItem("trenutniKorisnik"));
        var knjige = JSON.parse(window.localStorage.getItem("knjige"));
        var trazenaKnjiga = undefined;

        var kolicina = document.getElementById("kolicina").value;
        kolicina = parseInt(kolicina);

        for(knjiga of knjige){
            if(knjiga.id == window.location.search.substring(1)){
                if(kolicina > knjiga.kolicina){
                    window.alert("Nema toliko knjiga na lageru.");
                    return;
                } else {
                    trazenaKnjiga = knjiga;
                };
                break;
            };
        };

        for(korisnik of registrovaniKorisnici){
            if(korisnik.korisnickoIme == trenutniKorisnik.korisnickoIme){
                for(let i = 0; i < kolicina; i++){
                    trenutniKorisnik.korpa.push(trazenaKnjiga);
                    korisnik.korpa.push(trazenaKnjiga);
                };
                break;
            };
        };

        window.localStorage.setItem("registrovaniKorisnici", JSON.stringify(registrovaniKorisnici));
        window.localStorage.setItem("trenutniKorisnik", JSON.stringify(trenutniKorisnik));
        window.location.replace("korpa.html")
    })(document);
};